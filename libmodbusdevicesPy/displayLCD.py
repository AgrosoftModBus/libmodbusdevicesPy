import minimalmodbus

dictable =  list(u"ěščřžýáíéúůóťďňĚŠČŘŽÝÁÍÉÚŮŤĎŇ")
nedictable = list("escrzyaieuuotdnESCRZYAIEUUTDN")

class DisplayLCD(minimalmodbus.Instrument):
    def __init__(self, portname, slaveaddress):
        minimalmodbus.Instrument.__init__(self, portname, slaveaddress)
        self.serial.timeout = 0.1
        self.serial.baudrate = 9600

    def trans(self, a):
        if ord(a) > 127:
            if a in dictable:
                a = nedictable[dictable.index(a)]
            else:
                a = '?'
        return a

    def write(self, line0, line1):
        line = line0.ljust(16)[:16] + line1.ljust(16)[:16]
        a = ''
        for i in range(16):
            a = a + self.trans(line[2*i + 1]) + self.trans(line[2*i])

        self.write_string(0, a, number_of_registers=16)

if __name__ == '__main__':
    from time import sleep
    d = DisplayLCD('/dev/rs485_0005', 34)
    d.write('0123456789abcdef', '0123456789ABCDEF')
    sleep(1)
    d.write('01', '01')
    sleep(1)
    d.write('Žluťoučký kůň pě', 'l Ďábelské ody')
