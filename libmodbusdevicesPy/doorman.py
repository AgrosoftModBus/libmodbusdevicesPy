import minimalmodbus

class Doorman(minimalmodbus.Instrument):
    def __init__(self, portname, slaveaddress):
        minimalmodbus.Instrument.__init__(self, portname, slaveaddress)
        self.serial.timeout = 0.1
        self.serial.baudrate = 9600

    def readTransponder(self):
        data = self.read_registers(0, 4, 4)
        tr = (format(data[3], 'x') + format(data[2], 'x') + format(data[1], 'x') + format(data[0], 'x')).lstrip("0")
        if tr != '':
            self.write_register(0, 1, 0, 6)
        else:
            tr = None
        return tr

    def setRele(self, rele, state):
        '''
        rele 1 az 3
        state 0 vyp, 1 zap
        '''
        self.write_register(rele, state, 0, 6)


if __name__ == '__main__':
    from time import sleep
    d = Doorman('/dev/rs485_0005', 48)
    d.setRele(1, 1)
    sleep(1)
    d.setRele(2, 1)
    sleep(1)
    d.setRele(3, 1)
    sleep(1)
    d.setRele(1, 0)
    sleep(1)
    d.setRele(2, 0)
    sleep(1)
    d.setRele(3, 0)
